<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'driver_id',
        'from_address',
        'to_address',
        'items',
        'shipping_cost',
        'discount',
        'status_code',
        'completed_at',
        'driver_rate',
        'user_note',
        'driver_note',
        'driver_accept_at'
    ];

    protected $casts = [
        'from_address' => 'array',
        'to_address' => 'array',
        'items' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function status()
    {
        return $this->hasOne(Tracker::class)->latest();
    }

    public function tracker()
    {
        return $this->hasMany(Tracker::class);
    }
}
