<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * @OA\Get(
     *      path="/profile",
     *      operationId="getProfile",
     *      tags={"Profile"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function getProfile(Request $request)
    {
        return $request->user();
    }

    /**
     * @OA\Post(
     *      path="/profile",
     *      operationId="loginProfile",
     *      tags={"Profile"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'phone_number' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        auth()->user()->update($request->only([
            'name', 'phone_number'
        ]));

        return response([], 204);
    }

    public function changePassword(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'bail|required|string|min:6|confirmed',
            ], [
                'required' => config('errors.code.validation')['required'],
                'string' => config('errors.code.validation')['string'],
                'min' => config('errors.code.validation')['min'],
                'confirmed' => config('errors.code.validation')['confirmed'],
            ]);

            if ($validator->fails()) {
                return response([
                    'error' => true,
                    'errorCode' => $validator->messages()
                ], 422);
            }

            $request['password'] = bcrypt($request['password']);

            auth()->user()->update([
                'password' => $request['password']
            ]);

            return response([], 204);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [
                    $e->getMessage()
                ],
                'errorCode' => 500
            ], 500);
        }
    }
}
