<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * @OA\Get(
     *      path="/driver/orders/summary",
     *      operationId="summary",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function summary(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to' => 'required|date|after_or_equal:from',
            'status' => 'nullable|numeric'
        ]);

        if ($validator->fails()) {
            return response([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        $summary = Order::where('created_at', '>=', $request['from'])
            ->where('created_at', '<=', $request['to'])
            ->when($request['status'], function ($query) use ($request) {
                $query->where('status_code', $request['status']);
            })->get();

        return response([
            'data' => $summary,
        ]);
    }

    /**
     * @OA\Post(
     *      path="/driver/orders/{orderId}/accept",
     *      operationId="acceptOrder",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function acceptOrder(Request $request, Order $order)
    {
        if ($order->driver_id != auth('dirver')->id()) {
            return response([
                'error' => true,
                'message' => 'Unauthorized'
            ], 401);
        }

        $order->update([
            'driver_accept_at' => now(),
            'status_code' => config('const.order.status.inprocess')
        ]);

        return response([], 204);
    }

    /**
     * @OA\Post(
     *      path="/driver/orders/{orderId}/decline",
     *      operationId="declineOrder",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function declineOrder(Request $request, Order $order)
    {
        if ($order->driver_id != auth('dirver')->id()) {
            return response([
                'error' => true,
                'message' => 'Unauthorized'
            ], 401);
        }

        $order->update([
            'status_code' => config('const.order.status.cancled_by_driver')
        ]);

        return response([], 204);
    }
}
