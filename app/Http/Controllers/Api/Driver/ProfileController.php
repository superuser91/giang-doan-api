<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\DriverProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * @OA\Get(
     *      path="/driver/profile",
     *      operationId="driverprofile",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function profile(Request $request)
    {
        return response()->json([
            'data' => $request->user('driver')->load('profile')
        ]);
    }

    /**
     * @OA\Get(
     *      path="/driver/notifications",
     *      operationId="notifications",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function notifications(Request $request)
    {
        return response()->json([
            'data' => $request->user('driver')->notifications
        ]);
    }

    /**
     * @OA\Post(
     *      path="/driver/profile",
     *      operationId="driverupdateProfile",
     *      tags={"driver"},
     *      summary="",
     *      description="",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=422, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gplx_front_url' => 'required|url',
            'gplx_back_url' => 'required|url',
            'baohiem_url' => 'required|url',
            'cmnd_front_url' => 'required|url',
            'cmnd_back_url' => 'required|url',
            'reference_code' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
            return response([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        $profile = DriverProfile::updateOrCreate([
            'driver_id' => auth('driver')->id()
        ], $request->only([
            'gplx_front_url',
            'gplx_back_url',
            'baohiem_url',
            'cmnd_front_url',
            'cmnd_back_url',
            'reference_code',
        ]));

        return response()->json([
            'data' => $profile
        ]);
    }
}
